$(document).ready(function(){
	$(".progress-bar-danger").animate({"width":"125"},500).html("30%");
	$(".progress-bar-warning").animate({"width":"50"},1000).html("10%");
	$(".progress-bar-success").animate({"width":"375"},1500).html("70%");
	$(".progress-bar-primary").animate({"width":"470"},2000).html("90%");
	$(".progress-bar-info").animate({"width":"320"},2500).html("65%");
});
