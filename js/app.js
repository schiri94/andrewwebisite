$(document).ready(function(){
	
	$(".sidebar ul").hide();

	$(".btn_menu").click(function(){
		$(".sidebar ul").slideDown();
	});


	$("#form button").click(function(){
		var nome = $("#nome").val();
		var email = $("#email").val();
		var descrizione = $("#descrizione").val();
		
		if(nome==""){
			$("#nome").addClass("error");
		}else{
			$("#nome").removeClass("error");
			$("#nome").addClass("correct");
		}

		if(descrizione==""){
			$("#descrizione").addClass("error");
		}else{
			$("#descrizione").removeClass("error");
			$("#descrizione").addClass("correct");
		}

		if(email==""){
			$("#email").addClass("error");
		}else{
			if(validateEmail(email)){
				$("#email").removeClass("error");
				$("#email").addClass("correct");	
			}else{
				alert("Formato non valido!");
			}
			
		}

		if((email!=""&&validateEmail(email))&&nome!=""&&descrizione!=""){
			$('#myModal').modal('show'); 
		}
	});
});




function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}